# include <limits.h>
# include <string.h>
#include <stdio.h>
#include<iostream>
#include<fstream>
#include <sstream>
#include <stdlib.h>
#include <math.h>
#include <chrono>



using namespace std;
#define MAX_THREADS_PER_BLOCK 512

int no_of_nodes = 10;
int edge_list_size = 10;
int  f = 0, r = -1;

int  *a, *visited, q[100000];
int  *a1, *visited1;
int v1[30][100];

void bfs(int v) {
	for (int i = 1; i <= no_of_nodes - 1; i++)
	if (*(a + v*no_of_nodes + i) && !*(visited + i))
		q[++r] = i;
	if (f <= r) {
		*(visited + q[f]) = 1;
		bfs(q[f++]);
	}
}
int realibity(int v) {
	int version = 5;

	for (int i = 0; i < version; i++)
	{
		bfs(v);
		for (int j = 1; j <= no_of_nodes - 1; j++)
		if (*(visited + i))
			v1[i][j] = 1;
		else
			v1[i][j] = 0;

	}

	for (int i = 0; i < version; i++)
	{
		for (int i1 = 0; i1 < version; i1++)
		for (int j = 1; j <= no_of_nodes - 1; j++)
		if (v1[i][j] != v1[i1][j])
			return 0;
	}
	return 1;

}

int main()
{

	printf("\n Enter the number of vertices:");
	cin >> no_of_nodes;
	no_of_nodes = no_of_nodes + 1;
	visited = (int*)malloc(sizeof(int)*no_of_nodes);
	a = (int *)malloc(no_of_nodes * no_of_nodes * sizeof(int));
	bool stop;
	stop = false;
    int v;
	int h = 0;

	for (int i = 1; i <= no_of_nodes - 1; i++) {
		q[i] = 0;
		*(visited + i) = 0;
	}
	printf("\n Enter graph data in matrix form:\n");
	for (int i = 1; i <= no_of_nodes - 1; i++)
	for (int j = 1; j <= no_of_nodes - 1; j++)
	{
		if (i != j)
		{

			printf("a[%d][%d]:", i, j);
			cin >> h;
			*(a + i*no_of_nodes + j) = h;
		}
		else
			*(a + i*no_of_nodes + j) = 0;
	}

	printf("\n Enter the starting vertex:");
	cin >> v;
	auto start = chrono::steady_clock::now();
	int check_realibity = realibity(v);
	if (check_realibity == 1)
	{
		printf("\n The node which are reachable are:\n");
		for (int i = 1; i <= no_of_nodes - 1; i++)
		if (v1[0][i] == 1)
			printf("%d\t", i); else
			printf("\n Bfs is not possible");
	}
	else
		printf("\n result is not realibity");
	auto end = chrono::steady_clock::now();
	cout << "\nElapsed time in milliseconds : "
		<< chrono::duration_cast<chrono::milliseconds>(end - start).count()
		<< " ms" << endl;
	getchar();

	getchar();
	////////////////////////////////////////////////////////////////



	free(visited);
	free(a);

	
	return 0;
}
